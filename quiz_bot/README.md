# Quiz-bot
Bot is able to take quizzes, memorize their contents (using MongoDB),
offer quizzes in inline query and send them to the group.
And also track the answers to the quiz, and stop after a certain number of winners.

### Setup
- Install and configure MongoDB
- In `./app/db_utils.py` on line 24 write your mongo-url:
  ```
  self.client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
  ```
- Run the bot: `python server.py`

### Usage
##### Adding a quiz
- After entering the `/start` command, the bot will display a button to create the quiz
![result of `/game`](../.readme_static/quiz1.png)
- Click, fill in and send<br>
![result of `/game`](../.readme_static/quiz2.png)
- The bot will catch the quiz and save it to storage
![result of `/game`](../.readme_static/quiz3.png)
##### Quiz sending
- In the dialog with the bot or in the chat with the added bot, enter `@name_of_your_bot`,
The bot will display a list of saved quizzes in inline mode<br>
![result of `/game`](../.readme_static/quiz4.png)<br>
- After selecting a quiz, a message will be sent to the chat,
with an attached button to send the quiz (click and select chat)
![result of `/game`](../.readme_static/quiz5.png)
- In the chat will be added a bot that will send a quiz
![result of `/game`](../.readme_static/quiz6.png)
##### Summarizing
- After the number of correct answers exceeds 3, the bot will stop the poll
and summarize the results<br>
![result of `/game`](../.readme_static/quiz7.png)

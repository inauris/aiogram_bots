from aiogram import Dispatcher, types  # type: ignore
from ..db_utils import QuizesDB
from aiogram.utils import deep_linking  # type: ignore


async def inline_handler(query: types.InlineQuery) -> None:
    results = []
    user_quizzes = QuizesDB().get_user_quizes(str(query.from_user.id))

    if user_quizzes:
        for quiz in user_quizzes:
            keyboard = types.InlineKeyboardMarkup()
            start_quiz_button = types.InlineKeyboardButton(
                text="Отправить в группу",
                url=await deep_linking.get_startgroup_link(quiz["_id"]),
            )
            keyboard.add(start_quiz_button)
            results.append(
                types.InlineQueryResultArticle(
                    id=quiz["_id"],
                    title=quiz["question"],
                    input_message_content=types.InputTextMessageContent(
                        message_text="Нажмите кнопку ниже, чтобы "
                        "отправить викторину в группу."
                    ),
                    reply_markup=keyboard,
                )
            )
    await query.answer(
        switch_pm_text="Создать викторину",
        switch_pm_parameter="_",
        results=results,
        cache_time=120,
        is_personal=True,
    )


def register_inline_handlers(dp: Dispatcher) -> None:
    dp.register_inline_handler(inline_handler)

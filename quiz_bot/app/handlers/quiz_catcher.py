from aiogram import Dispatcher, types  # type: ignore

from ..db_utils import QuizEntry, QuizesDB


def _convert_to_quiz_obj(message: types.Message) -> QuizEntry:
    return QuizEntry(
        {
            "_id": message.poll.id,
            "owner_id": str(message.from_user.id),
            "question": message.poll.question,
            "options": [o.text for o in message.poll.options],
            "correct_option_id": message.poll.correct_option_id,
        }
    )


async def msg_with_poll(message: types.Message) -> None:
    if message.poll.type != "quiz":
        await message.reply("Извините, я принимаю только викторины (quiz)!")
        return
    quiz_db = QuizesDB()
    quizEntry = _convert_to_quiz_obj(message)
    quiz_db.insert_quiz(quizEntry)

    await message.reply(
        "Викторина сохранена. Общее число сохранённых викторин: "
        f"{quiz_db.count_quizes(str(message.from_user.id))}"
    )


def register_msgPoll_handlers(dp: Dispatcher) -> None:
    dp.register_message_handler(msg_with_poll, content_types=["poll"])

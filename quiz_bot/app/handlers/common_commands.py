from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher.filters import Text  # type: ignore

from ..db_utils import GameSession, QuizesDB


async def cmd_start(message: types.Message) -> None:
    if message.chat.type == types.ChatType.PRIVATE:
        poll_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        poll_keyboard.add(
            types.KeyboardButton(
                text="Создать викторину",
                request_poll=types.KeyboardButtonPollType(
                    type=types.PollType.QUIZ
                ),
            )
        )
        poll_keyboard.add(types.KeyboardButton(text="Отмена"))
        await message.answer(
            "Нажмите на кнопку ниже и создайте викторину!",
            reply_markup=poll_keyboard,
        )
    else:
        # words[0] = start, words[1] = QuizEntry._id
        words = message.text.split()
        # If the /start or /startgroup command has a parameter,
        # this is probably the quiz ID. Check it and send it.
        if len(words) == 1:
            bot_info = await message.bot.get_me()

            keyboard = types.InlineKeyboardMarkup()
            move_to_dm_button = types.InlineKeyboardButton(
                text="Перейти в ЛС",
                url=f"t.me/{bot_info.username}?start=anything",
            )
            keyboard.add(move_to_dm_button)
            await message.reply(
                "Не выбрана ни одна викторина. Пожалуйста, перейдите в личные "
                "сообщения с ботом, чтобы создать новую.",
                reply_markup=keyboard,
            )
        else:
            quiz_db = QuizesDB()
            quizEntry = quiz_db.get_quiz(words[1])
            if not quizEntry:
                await message.reply(
                    "Викторина удалена или недействительна. "
                    "Попробуйте создать новую."
                )
                return
            msg = await message.bot.send_poll(
                chat_id=message.chat.id,
                question=quizEntry["question"],
                is_anonymous=False,
                options=quizEntry["options"],
                type="quiz",
                correct_option_id=quizEntry["correct_option_id"],
            )

            game_session = GameSession(
                {
                    "_id": msg.poll.id,
                    "correct_option_id": quizEntry["correct_option_id"],
                    "winners": [],
                    "chat_id": msg.chat.id,
                    "message_id": msg.message_id,
                }
            )
            quiz_db.set_game_session(game_session)


async def cmd_cancel(message: types.Message) -> None:
    remove_keyboard = types.ReplyKeyboardRemove()
    await message.answer(
        "Действие отменено. Введите /start, чтобы начать заново.",
        reply_markup=remove_keyboard,
    )


def register_handlers_common(dp: Dispatcher) -> None:
    dp.register_message_handler(cmd_start, commands="start")
    dp.register_message_handler(cmd_cancel, commands="cancel")
    dp.register_message_handler(
        cmd_cancel, Text(equals="отмена", ignore_case=True)
    )

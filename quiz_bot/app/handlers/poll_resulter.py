import logging

from aiogram import Dispatcher, types  # type: ignore

from ..db_utils import QuizesDB


async def check_answers(quiz_answer: types.PollAnswer):
    """Adds the correct users to the list of winners (+ closes the quiz)"""
    quiz_db = QuizesDB()
    game_session = quiz_db.get_game_session(quiz_answer.poll_id)
    if not game_session:
        logging.error(
            "Не могу найти викторину с таким id"
            f"quiz_answer.poll_id = {quiz_answer.poll_id}"
        )
        return
    # quiz_answer.option_ids[0] is number of correct option
    if game_session["correct_option_id"] == quiz_answer.option_ids[0]:
        quiz_db.append_winner_to_game_session(
            game_session["_id"], quiz_answer.user.id
        )
        # current + new appended
        number_of_winners = len(game_session["winners"]) + 1

        if number_of_winners == 3:
            await quiz_answer.bot.stop_poll(
                game_session["chat_id"], game_session["message_id"]
            )


async def announce_the_results(quiz: types.Poll):
    """
    At the close of the quiz sends a list of winners,
    and closes the game session
    """
    quizes_db = QuizesDB()
    game_session = quizes_db.get_game_session(quiz.id)
    if not game_session:
        logging.error(
            "Не могу найти викторину с таким id"
            f"с quiz.id = {quiz.id}"
        )
        return
    # Form final message
    congrats_text = []
    for winner in game_session["winners"]:
        chat_member_info = await quiz.bot.get_chat_member(
            game_session["chat_id"], winner
        )
        congrats_text.append(chat_member_info.user.get_mention(as_html=True))

    await quiz.bot.send_message(
        game_session["chat_id"],
        "Викторина закончена, всем спасибо! Вот наши победители:\n\n"
        + "\n".join(congrats_text),
        parse_mode="HTML",
    )
    quizes_db.finish_game_session(game_session["_id"])


def register_poll_handlers(dp: Dispatcher) -> None:
    dp.register_poll_answer_handler(check_answers)
    dp.register_poll_handler(
        announce_the_results, lambda quiz: quiz.is_closed is True
    )

import pymongo  # type: ignore

from typing import TypedDict


class QuizEntry(TypedDict):
    _id: str
    owner_id: str
    question: str
    options: list[str]
    correct_option_id: int


class GameSession(TypedDict):
    _id: str
    correct_option_id: int
    winners: list[int]
    chat_id: int
    message_id: int


class QuizesDB:
    def __init__(self):
        self.client = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
        self.db = self.client["quiz_bot"]
        self.quizes = self.db["quizes"]
        self.game_sessions = self.db["game_sessions"]
        self._check_db_exists()

    def insert_quiz(self, quiz: QuizEntry) -> None:
        self.quizes.insert_one(quiz)

    def count_quizes(self, user_id: str) -> int:
        return self.quizes.count_documents({"owner_id": user_id})

    def get_user_quizes(self, user_id: str) -> list[QuizEntry]:
        return self.quizes.find({"owner_id": user_id})

    def get_quiz(self, _id: str) -> QuizEntry:
        return self.quizes.find_one({"_id": _id})

    def set_game_session(self, game_session: GameSession) -> None:
        self.game_sessions.insert_one(game_session)

    def append_winner_to_game_session(self, _id: str, winner_id: int) -> None:
        self.game_sessions.update_one(
            {"_id": _id}, {"$push": {"winners": winner_id}}
        )

    def get_game_session(self, _id: str) -> GameSession:
        return self.game_sessions.find_one({"_id": _id})

    def finish_game_session(self, _id: str) -> None:
        return self.game_sessions.delete_one({"_id": _id})

    def _check_db_exists(self) -> None:
        """If db isn't exist - will init"""
        collection_names = self.db.list_collection_names()
        if "quizes" not in collection_names:
            print("coll: quizes isn't exist.")
            self.quizes.create_index([("owner_id", pymongo.DESCENDING)])
            print("Index successfuly created!")

import asyncio
import logging

from aiogram import Bot, Dispatcher, executor, types  # type: ignore
from aiogram.utils.exceptions import BotBlocked  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


# Usage of dispatcher:
# -----------------------------------------------------------------------------
@dp.message_handler(commands="test1")
async def cmd_test1(message: types.Message):
    await message.reply("Test 1")


async def cmd_test2(message: types.Message):
    await message.reply("Test 2")

dp.register_message_handler(cmd_test2, commands="test2")


# Usage of answer_{type}:
# -----------------------------------------------------------------------------
@dp.message_handler(commands="dice")
async def cmd_dice(message: types.Message):
    await message.answer_dice(emoji="🎲")
    # In another chat: (Need "bot" object)
    # await message.bot.send_dice(-100123456789, emoji="🎲")


# Usage of errors handler
# -----------------------------------------------------------------------------
@dp.message_handler(commands="block")
async def cmd_block(message: types.Message):
    await asyncio.sleep(10.0)  # Здоровый сон на 10 секунд
    await message.reply("Вы заблокированы")


@dp.errors_handler(exception=BotBlocked)
async def error_bot_blocked(update: types.Update, exception: BotBlocked):
    # Update: объект события от Telegram. Exception: объект исключения
    # Здесь можно как-то обработать блокировку, например,
    # удалить пользователя из БД
    print(
        "Меня заблокировал пользователь!\n"
        f"Сообщение: {update}\nОшибка: {exception}"
    )

    # Такой хэндлер должен всегда возвращать True,
    # если дальнейшая обработка не требуется.
    return True


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

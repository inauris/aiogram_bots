# Youtube-bookmarker
Bot allows you to create bookmarks with your favorite YouTube videos and send them

#### Setup
- Create a bot and enable inline mode (`/setinline` in chat `@BotFather`)


#### Usage
- In the dialog with the bot or in the chat with the added bot, enter `@name_of_your_bot`,
The bot will display a list of saved links in inline mode, clicking on one of
which will send the corresponding `via @bot` message to the chat
![result of `/game`](../.readme_static/inline1.png)
![result of `/game`](../.readme_static/inline1_1.png)
- `/add`: add a new link
![result of `/game`](../.readme_static/inline2.png)
- `/links`: list of links
![result of `/game`](../.readme_static/inline3.png)
- `/cancel`: undo adding link

from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore
from aiogram.dispatcher.filters import Text  # type: ignore


async def cmd_start(message: types.Message, state: FSMContext) -> None:
    await state.finish()
    await message.answer(
        "Воспользуйтесь командой /add, чтобы добавить новую ссылку"
    )


async def cmd_cancel(message: types.Message, state: FSMContext) -> None:
    await state.finish()
    await message.answer("Действие отменено")


def register_handlers_common(dp: Dispatcher) -> None:
    dp.register_message_handler(cmd_start, commands="start", state="*")
    dp.register_message_handler(cmd_cancel, commands="cancel", state="*")
    dp.register_message_handler(
        cmd_cancel, Text(equals="отмена", ignore_case=True), state="*"
    )

from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore
from aiogram.dispatcher.filters import Text  # type: ignore
from ..DBManager import DBManager

dbworker = DBManager()


def _get_links_entry(user_id: int) -> (types.InlineKeyboardMarkup | str):
    """Entry is inline markup that attach to msg"""
    links = dbworker.get_links(user_id)
    if len(links) == 0:
        return "У вас нет сохранённых ссылок!"

    inline_kb = types.InlineKeyboardMarkup()
    for item in links:  # item[0]: yt_hash; item[1]: description
        inline_kb.add(
            types.InlineKeyboardButton(
                text=item[1], switch_inline_query_current_chat=f"{item[1]}"
            ),
            types.InlineKeyboardButton(
                text="Удалить!",
                callback_data=f"del~{item[0]}~{user_id}",
            ),
        )
    return inline_kb


def _valid_delete_link(call_data: str, call_user_id: int) -> tuple[bool, str]:
    args = call_data.split("~")
    if len(args) != 3:
        return (
            False,
            "неверный формат callback'a. Свяжитесь с администратором",
        )

    user_id = int(args[2])
    if user_id != call_user_id:
        return (False, "Вы не можете редактировать чужую запись")
    return (True, "")


async def view_links(message: types.Message, state: FSMContext) -> None:
    await state.finish()

    links_entry = _get_links_entry(message.from_user.id)
    if type(links_entry) != types.InlineKeyboardMarkup:
        return await message.answer(links_entry)
    await message.answer(
        text=(
            "Ваши сохраненные ссылки:\n(Левый столбик - демонстрирует "
            "работу ссылки, соответствующий правый - удаляет)"
        ),
        reply_markup=links_entry,
    )


async def delete_link(call: types.CallbackQuery, state: FSMContext) -> None:
    await state.finish()

    is_valid, error_msg = _valid_delete_link(call.data, call.from_user.id)
    if not is_valid:
        return await call.answer(error_msg)

    yt_hash = call.data.split("~")[1]
    dbworker.delete(call.from_user.id, yt_hash)

    links_entry = _get_links_entry(call.from_user.id)
    if type(links_entry) != types.InlineKeyboardMarkup:
        await call.message.edit_text(
            text="Вы удалили все сохраненные ссылки! "
            "Воспользуйтесь командой /add, чтобы добавить новую ссылку"
        )
    else:
        await call.message.edit_reply_markup(reply_markup=links_entry)
    await call.answer("Ссылка успешно удалена!")


def register_manage_links_handlers(dp: Dispatcher) -> None:
    dp.register_message_handler(view_links, commands="links", state="*")
    dp.register_callback_query_handler(
        delete_link, Text(startswith="del~"), state="*"
    )

import sqlite3
from typing import Any

from config import SQL_DUMP, DATABASE


class DBManager:
    def __init__(self, database=DATABASE):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()
        self._check_db_exists()

    def insert_or_update(
        self, user_id: int, youtube_hash: str, description: str
    ) -> None:
        statement = (
            "INSERT INTO youtube (user_id, youtube_hash, description) "
            "VALUES (:user_id, :youtube_hash, :description) "
            "ON CONFLICT(user_id, youtube_hash) "
            "DO UPDATE SET description = :description"
        )
        self.cursor.execute(
            statement,
            {
                "user_id": user_id,
                "youtube_hash": youtube_hash,
                "description": description,
            },
        )
        self.cursor.connection.commit()

    def delete(self, user_id: int, youtube_hash: str) -> None:
        statement = (
            "DELETE from youtube WHERE user_id = ? and youtube_hash = ?"
        )
        self.cursor.execute(statement, (user_id, youtube_hash))
        self.cursor.connection.commit()

    def get_links(self, user_id: int, search_query: str = None) -> list[Any]:
        statement = (
            "SELECT youtube_hash, description from youtube WHERE user_id = ?"
        )
        if search_query:
            statement += " AND description LIKE ?"
            result = self.cursor.execute(
                statement, (user_id, f"%{search_query}%")
            )
        else:
            result = self.cursor.execute(statement, (user_id,))
        return result.fetchall()

    def _init_db(self) -> None:
        """Инициализирует БД"""
        with open(SQL_DUMP, "r") as f:
            sql = f.read()
        self.cursor.executescript(sql)
        self.connection.commit()

    def _check_db_exists(self) -> None:
        """Проверяет, инициализирована ли БД, если нет — инициализирует"""
        self.cursor.execute(
            "SELECT name FROM sqlite_master "
            "WHERE type='table' AND name='youtube'"
        )
        table_exists = self.cursor.fetchall()
        if table_exists:
            return
        self._init_db()

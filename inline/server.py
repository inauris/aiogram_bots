import asyncio
import logging

from aiogram import Bot, Dispatcher  # type: ignore
from aiogram.contrib.fsm_storage.memory import MemoryStorage  # type: ignore
from aiogram.types import BotCommand  # type: ignore

from app.handlers.adding_links import register_add_links_handlers
from app.handlers.common_commands import register_handlers_common
from app.handlers.inline_mode import register_inline_handlers
from app.handlers.manage_links import register_manage_links_handlers
from config import TOKEN

logger = logging.getLogger(__name__)


async def set_commands(bot: Bot) -> None:
    commands = [
        BotCommand(command="/add", description="Добавить новую ссылку"),
        BotCommand(command="/links", description="Просмотр списка ссылок"),
        BotCommand(command="/cancel", description="Отменить текущее действие"),
    ]
    await bot.set_my_commands(commands)


async def del_commands(bot: Bot) -> None:
    await bot.delete_my_commands()


async def main() -> None:
    # Setting logging to stdout
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )
    logger.error("Starting bot")

    bot = Bot(token=TOKEN)
    dp = Dispatcher(bot, storage=MemoryStorage())

    register_manage_links_handlers(dp)
    register_handlers_common(dp)
    register_add_links_handlers(dp)
    register_inline_handlers(dp)

    await set_commands(bot)
    # await del_commands(bot)

    await dp.start_polling()


if __name__ == "__main__":
    asyncio.run(main())

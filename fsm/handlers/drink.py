from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore
from aiogram.dispatcher.filters.state import State, StatesGroup  # type: ignore

available_drink_names = ["Квас", "Березовый сок", "Медовуха"]
available_drink_sizes = ["500мл", "1л", "2л"]


class OrderDrink(StatesGroup):
    waiting_for_drink_name = State()
    waiting_for_drink_size = State()


async def drink_start(message: types.Message) -> None:
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for name in available_drink_names:
        keyboard.add(name)
    await message.answer("Выберите напиток:", reply_markup=keyboard)
    await OrderDrink.waiting_for_drink_name.set()


async def drink_chosen(message: types.Message, state: FSMContext) -> None:
    if message.text.lower() not in [x.lower() for x in available_drink_names]:
        await message.answer(
            "Пожалуйста, выберите напиток, используя клавиатуру ниже."
        )
        return
    await state.update_data(chosen_drink=message.text.lower())

    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for size in available_drink_sizes:
        keyboard.add(size)

    await OrderDrink.next()
    await message.answer(
        "Теперь выберите размер порции:", reply_markup=keyboard
    )


async def drink_size_chosen(message: types.Message, state: FSMContext) -> None:
    if message.text.lower() not in [x.lower() for x in available_drink_sizes]:
        await message.answer(
            "Пожалуйста, выберите необходимый обьем, используя клавиатуру ниже"
        )
        return
    user_data = await state.get_data()
    await message.answer(
        f"Вы заказали {user_data.get('chosen_drink')} "
        f"обьемом {message.text.lower()}.\n"
        f"Попробуйте теперь заказать еду: /food",
        reply_markup=types.ReplyKeyboardRemove(),
    )
    await state.finish()


def register_handlers_drink(dp: Dispatcher) -> None:
    dp.register_message_handler(drink_start, commands="drink", state="*")
    dp.register_message_handler(
        drink_chosen, state=OrderDrink.waiting_for_drink_name
    )
    dp.register_message_handler(
        drink_size_chosen, state=OrderDrink.waiting_for_drink_size
    )

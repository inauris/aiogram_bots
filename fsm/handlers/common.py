from aiogram import Dispatcher, types  # type: ignore
from aiogram.dispatcher import FSMContext  # type: ignore
from aiogram.dispatcher.filters import IDFilter, Text  # type: ignore


async def cmd_start(message: types.Message, state: FSMContext) -> None:
    await state.finish()
    await message.answer(
        "Выберите, что хотите заказать: напитки (/drink) или блюда (/food).",
        reply_markup=types.ReplyKeyboardRemove(),
    )


async def cmd_cancel(message: types.Message, state: FSMContext) -> None:
    await state.finish()
    await message.answer(
        "Действие отменено", reply_markup=types.ReplyKeyboardRemove()
    )


async def secret_command(message: types.Message):
    await message.answer(
        "Поздравляю! Эта команда доступна только администратору бота."
    )


def register_handlers_common(dp: Dispatcher, admin_id: str) -> None:
    dp.register_message_handler(cmd_start, commands="start", state="*")
    dp.register_message_handler(cmd_cancel, commands="cancel", state="*")
    dp.register_message_handler(
        cmd_cancel, Text(equals="отмена", ignore_case=True), state="*"
    )
    dp.register_message_handler(
        secret_command, IDFilter(user_id=admin_id), commands="abracadabra"
    )

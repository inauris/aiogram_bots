import asyncio
import logging

from aiogram import Bot, Dispatcher  # type: ignore
from aiogram.contrib.fsm_storage.memory import MemoryStorage  # type: ignore
from aiogram.contrib.fsm_storage.redis import RedisStorage2  # type: ignore
from aiogram.contrib.fsm_storage.mongo import MongoStorage  # type: ignore
from aiogram.types import BotCommand  # type: ignore

from config import ADMIN_ID, TOKEN
from handlers.common import register_handlers_common
from handlers.drink import register_handlers_drink
from handlers.food import register_handlers_food

logger = logging.getLogger(__name__)


async def set_commands(bot: Bot):
    commands = [
        BotCommand(command="/drink", description="Заказать напитки"),
        BotCommand(command="/food", description="Заказать блюда"),
        BotCommand(command="/cancel", description="Отменить текущее действие"),
    ]
    await bot.set_my_commands(commands)


async def del_commands(bot: Bot):
    await bot.delete_my_commands()


async def main():
    # Setting logging to stdout
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )
    logger.error("Starting bot")

    bot = Bot(token=TOKEN)
    # storage = MemoryStorage()
    # storage = RedisStorage2(
    #     "localhost", 6379, db=1, pool_size=10, prefix="my_fsm_key"
    # )
    storage = MongoStorage(host="localhost", port=27017, db_name="aiogram_fsm")
    dp = Dispatcher(bot, storage=storage)

    register_handlers_common(dp, ADMIN_ID)
    register_handlers_drink(dp)
    register_handlers_food(dp)

    await set_commands(bot)
    # await del_commands(bot)

    await dp.start_polling()

    await dp.storage.close()
    await dp.storage.wait_closed()


if __name__ == "__main__":
    asyncio.run(main())

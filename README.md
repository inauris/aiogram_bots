# aiogram_bots 
Bots written with the aiogram library

### Pay attention to:
- [Youtube-bookmarker](#youtube_bookmarker)
- [Quiz-bookmarker](#quiz_bookmarker)

---

<a name='youtube_bookmarker'></a>
#### [Youtube-bookmarker](./inline)
Bot allows you to create bookmarks with your favorite YouTube videos and send them
![result of `/game`](./.readme_static/inline1.png)<br>
![result of `/game`](./.readme_static/inline1_1.png)

---

<a name='quiz_bookmarker'></a>
#### [Quiz-bookmarker](./quiz_bot)
Bot is able to take quizzes, memorize their contents (using MongoDB),
offer quizzes in inline query and send them to the group.
And also track the answers to the quiz, and stop after a certain number of winners.

![result of `/game`](./.readme_static/quiz4.png)<br>
![result of `/game`](./.readme_static/quiz5.png)<br>
![result of `/game`](./.readme_static/quiz6.png)<br>
![result of `/game`](./.readme_static/quiz7.png)

---

## Setup
To start you need:
1. Download the repository and install the dependencies:
    ```
    git clone https://gitlab.com/inauris/aiogram_bots.git
    cd aiogram_bots
    poetry env use python
    source $(poetry env info --path)/bin/activate && poetry install
    ```
2. In the root of the project create a file `./.env`:
    ```
    echo "# Bot token from @BotFather
    TOKEN = 'YOUR_BOT_TOKEN'
    # Admin telegram user_id
    ADMIN_ID=000000000" > .env
    ```
    And replace token and admin_id with yours
3. Run bots:
    ```
    # Youtube-bookmarker
    python inline/server.py

    # Quiz bot
    python quiz_bot/server.py
    ```
> p.s. Do not forget to perform some settings for each particular bot
(Specified in corresponding bot's directories)

"""
My method without any storage
"""
import logging

from aiogram import Bot, Dispatcher, executor, types  # type: ignore
from aiogram.utils.callback_data import CallbackData  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
callback_numbers = CallbackData("fabnum", "action")
dp = Dispatcher(bot)


def _get_kb() -> types.InlineKeyboardMarkup:
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    buttons = [
        types.InlineKeyboardButton(
            text="-1", callback_data=callback_numbers.new(action="decr")
        ),
        types.InlineKeyboardButton(
            text="+1", callback_data=callback_numbers.new(action="incr")
        ),
        types.InlineKeyboardButton(
            text="Подтвердить",
            callback_data=callback_numbers.new(action="finish"),
        ),
    ]
    keyboard.add(*buttons)
    return keyboard


async def _update_number(message: types.Message, new_value: int) -> None:
    await message.edit_text(
        f"Укажите число: {new_value}", reply_markup=_get_kb()
    )


@dp.message_handler(commands=["crement"])
async def send_kb(message: types.Message) -> None:
    await message.answer("Укажите число: 0", reply_markup=_get_kb())


@dp.callback_query_handler(callback_numbers.filter(action=["incr", "decr"]))
async def crement(call: types.callback_query, callback_data: dict) -> None:
    number = int(call.message.text.split()[2])
    action = callback_data["action"]

    if action == "decr":
        number -= 1
    elif action == "incr":
        number += 1
    await _update_number(call.message, number)
    await call.answer()


@dp.callback_query_handler(callback_numbers.filter(action=["finish"]))
async def get_result(call: types.callback_query) -> None:
    number = int(call.message.text.split()[2])
    await call.message.edit_text(f"Итого получилось: {number}")
    await call.answer()


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

"""
ReplyKeyboardMarkup: simle and special buttons(location, contact, poll)
"""
import logging

from aiogram import Bot, Dispatcher, executor, types  # type: ignore
from aiogram.dispatcher.filters import Text  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


# -----------------------------------------------------------------------------
# ReplyKeyboardMarkup
# -----------------------------------------------------------------------------

# Simple buttons:
# ------------------------------------------
@dp.message_handler(commands=["start"])
async def start_kb(message: types.Message) -> None:
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    button_cold = types.KeyboardButton(text="Холодным")
    buttons = [button_cold, "Горячим", "/stop"]
    keyboard.add(*buttons)
    await message.answer("Как подавать каркадэ?", reply_markup=keyboard)


# One way of filtering (aiogram.dispatcher.filters.Text)
@dp.message_handler(Text(equals="Холодным"))
async def cold_tea(message: types.Message) -> None:
    await message.reply(
        "ДААААААААААААААААААААААА. Отличный выбор!!!",
        reply_markup=types.ReplyKeyboardRemove(),
    )


# Another(preferred) way of filtering (lambda)
@dp.message_handler(lambda message: message.text == "Горячим")
async def hot_tea(message: types.Message) -> None:
    await message.reply(
        "Мы надеемся что вы просто занятой человек...",
        reply_markup=types.ReplyKeyboardRemove(),
    )


@dp.message_handler(commands=["stop"])
async def stop_kb(message: types.Message) -> None:
    await message.reply(
        "Прекратили",
        reply_markup=types.ReplyKeyboardRemove(),
    )


# Special buttons:
# ------------------------------------------
# Objects KeyboardButton have 3 key arguments:
# request_{location, contact, poll}
@dp.message_handler(commands="special_buttons")
async def cmd_special_buttons(message: types.Message) -> None:
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    location_button = types.KeyboardButton(
        text="Запросить геолокацию", request_location=True
    )
    contact_button = types.KeyboardButton(
        text="Запросить контакт", request_contact=True
    )
    poll_button = types.KeyboardButton(
        text="Создать викторину",
        request_poll=types.KeyboardButtonPollType(type=types.PollType.QUIZ),
    )
    keyboard.add(location_button, contact_button, poll_button, "/stop")
    await message.answer("Выберите действие:", reply_markup=keyboard)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

"""
Updated Groosha's ways with CallbackData and message_id as "key" in user_data{}
"""
import logging
from contextlib import suppress

from aiogram import Bot, Dispatcher, executor, types  # type: ignore
from aiogram.utils.callback_data import CallbackData  # type: ignore
from aiogram.utils.exceptions import MessageNotModified  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


callback_numbers = CallbackData("fabnum", "action")
MsgId = str
user_data: dict[MsgId, int] = {}


def _get_kb() -> types.InlineKeyboardMarkup:
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    buttons = [
        types.InlineKeyboardButton(
            text="-1", callback_data=callback_numbers.new(action="decr")
        ),
        types.InlineKeyboardButton(
            text="+1", callback_data=callback_numbers.new(action="incr")
        ),
        types.InlineKeyboardButton(
            text="Подтвердить",
            callback_data=callback_numbers.new(action="finish"),
        ),
    ]
    keyboard.add(*buttons)
    return keyboard


async def _update_number(message: types.Message, new_value: int) -> None:
    with suppress(MessageNotModified):
        await message.edit_text(
            f"Укажите число: {new_value}", reply_markup=_get_kb()
        )


@dp.message_handler(commands=["crement"])
async def send_kb(message: types.Message) -> None:
    user_data[message.message_id] = 0
    await message.answer("Укажите число: 0", reply_markup=_get_kb())


@dp.callback_query_handler(callback_numbers.filter(action=["incr", "decr"]))
async def crement(call: types.callback_query, callback_data: dict) -> None:
    number = user_data.get(call.message.message_id, 0)
    action = callback_data["action"]

    if action == "decr":
        number -= 1
    elif action == "incr":
        number += 1
    user_data[call.message.message_id] = number
    await _update_number(call.message, number)
    await call.answer()


@dp.callback_query_handler(callback_numbers.filter(action=["finish"]))
async def callbacks_num_finish_fab(call: types.callback_query) -> None:
    # num = types.callback_query.CallbackQuery.message.message_id
    number = user_data.pop(call.message.message_id, 0)
    await call.message.edit_text(f"Итого получилось: {number}")
    await call.answer()


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

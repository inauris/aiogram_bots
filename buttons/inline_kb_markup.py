"""
InlineKeyboardMarkup: callback and url (without switch, login & pay)
"""
import logging
from random import randint

from aiogram import Bot, Dispatcher, executor, types  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


# Url buttons:
# ------------------------------------------
# For creation just add pos_arg (,url="")
@dp.message_handler(commands=["inline_url"])
async def start_kb(message: types.Message) -> None:
    kb = types.InlineKeyboardMarkup(row_width=2)
    buttons = [
        types.InlineKeyboardButton(
            text="Хабр", url="https://habr.com/ru/all/"
        ),
        types.InlineKeyboardButton(
            text="Волк в овечей шкуре!", url="https://github.com"
        ),
        types.InlineKeyboardButton(
            text="Оф. канал Telegram", url="tg://resolve?domain=telegram"
        ),
    ]
    kb.add(*buttons)
    await message.answer("Получай ответ!", reply_markup=kb)


# Callback buttons:
# ------------------------------------------
# For creation just add pos_arg (,callback_data="")
# Objects KeyboardButton have 3 key arguments:
# request_{location, contact, poll}
@dp.message_handler(commands="random")
async def cmd_random(message: types.Message) -> None:
    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton(text="Press on me", callback_data="random")
    )
    await message.answer(
        "Нажмите на кнопку, чтоб бот отправил случайно число от 1 до 10",
        reply_markup=kb,
    )


@dp.callback_query_handler(text="random")
async def send_random_value(call: types.CallbackQuery) -> None:
    # send a message
    await call.message.answer(str(randint(1, 10)))
    # userinfo with callback: call.from_user
    await call.message.answer(
        f"username: {call.from_user.username}, userId: {call.from_user.id}"
    )
    await call.answer()
    # or
    # await call.answer(text='Получайте ваше число!', show_alert=False)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

import logging
from os.path import dirname, join

import aiogram.utils.markdown as fmt  # type: ignore
from aiogram import Bot, Dispatcher, executor, types  # type: ignore

from config import TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
# bot = Bot(token=TOKEN, parse_mode=types.ParseMode.HTML)
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


# Download files by server
# -----------------------------------------------------------------------------
@dp.message_handler(content_types=[types.ContentType.DOCUMENT])
async def download_doc(message: types.Message):
    # Скачивание в каталог с ботом с созданием подкаталогов по типу файла
    await message.document.download(
        # destination_dir="/home/inauris/projects/aiogram_bots/messages/temp"
        destination_dir=join(dirname(__file__), "./subdir")
    )


# Типы содержимого тоже можно указывать по-разному.
@dp.message_handler(content_types=["photo"])
async def download_photo(message: types.Message):
    # Убедитесь, что каталог /tmp/somedir существует!
    await message.photo[-1].download(
        # destination_file="/path/to/file/name.png"
        destination_dir=dirname(__file__)
    )


# link preview as a photo replacement (hide_link: empty space)
# (photo caption have text limit 1024 chars, just text - 4096)
# -----------------------------------------------------------------------------
@dp.message_handler(commands="test4")
async def with_hidden_link(message: types.Message):
    await message.answer(
        f"{fmt.hide_link('https://habr.com/ru/all/')}Просто большой текст. Кто"
        "мог подумать, что в 2020 году в Telegram появятся видеозвонки!\n\n"
        "Обычные голосовые вызовы возникли в Telegram лишь в 2017, заметно"
        "позже своих конкурентов. А спустя три года, когда огромное количество"
        "людей на планете приучились работать из дома из-за эпидемии "
        "коронавируса, команда Павла Дурова не растерялась и сделала"
        "качественные видеозвонки на WebRTC!\n\n"
        "P.S. а ещё ходят слухи про демонстрацию своего экрана :)",
        parse_mode=types.ParseMode.HTML,
    )


# U can use file_id received files
# -----------------------------------------------------------------------------
@dp.message_handler(content_types=[types.ContentType.ANIMATION])
async def echo_document(message: types.Message):
    await message.reply_animation(message.animation.file_id)
    await message.answer(message.animation.file_id)


@dp.message_handler(commands=["sti"])
async def split(message: types.Message):
    await message.reply_animation(
        "CgACAgUAAxkBAAIHmWKXGHDgk9A4eVeIQf"
        "SBY6mOlbDIAALkAQACUzUBVUSN7eUTs5CvJAQ"
    )


# Form message with diff ways parse_mode
# -----------------------------------------------------------------------------
# @dp.message_handler()
# async def parse(message: types.Message):
#     await message.answer(f"DEfault <b>{message.text}</b>!")
#     await message.answer(f"empty <b>{message.text}</b>!", parse_mode="")
#     await message.answer(
#         r"markwodn, ~world~\!", parse_mode=types.ParseMode.MARKDOWN_V2
#     )
#     await message.answer(
#         r"markwodn, *world*\!", parse_mode=types.ParseMode.MARKDOWN_V2
#     )
#
#     await message.answer(
#         fmt.text(
#             fmt.text(fmt.hunderline("Яблоки"), ", вес 1 кг."),
#             fmt.text("Старая цена:", fmt.hstrikethrough(50), "рублей"),
#             fmt.text("Новая цена:", fmt.hbold(25), "рублей"),
#             fmt.text(fmt.spoiler('asdfasdfasdf', '~-./[]')),
#             sep="\n",
#         ),
#         parse_mode="HTML",
#     )


# -----------------------------------------------------------------------------
# an invalid parsemode message will throw an error. To avoid the error,
# you must escape some characters (e.g. tags in markdwon).
# If we want to handle user message use following methods:
# .escape_md() - Shields problematic characters
# .quote_html - Shields problematic characters
# .(h)bold
# .(h)italic
# .etc()
@dp.message_handler()
async def asdfd(message):
    await message.answer(
        f"Привет {fmt.italic(message.text)}",
        parse_mode=types.ParseMode.MARKDOWN_V2,
    )
    # or
    # await message.answer(
    #     f"Привет, <i>{fmt.quote_html(message.text)}</i>",
    #     parse_mode=types.ParseMode.HTML,
    # )


# Get message with client parsing
# -----------------------------------------------------------------------------
# @dp.message_handler()
# async def parse(message: types.Message):
#     await message.answer(message.text)
#     await message.answer(message.md_text)
#     await message.answer(message.html_text)
#     # Дополняем исходный текст:
#     await message.answer(
#         f"<u>Ваш текст</u>:\n\n{message.html_text}", parse_mode="HTML"
#     )


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
